package com.demo.antlr;

import com.demo.antlr.test.ArrayInitLexer;
import com.demo.antlr.test.ArrayInitParser;
import org.antlr.v4.gui.TestRig;
import org.antlr.v4.gui.Trees;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.UnbufferedCharStream;

import java.io.ByteArrayInputStream;
import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {

        //创建词法分析器
        ArrayInitLexer arrayInitLexer = new ArrayInitLexer(CharStreams.fromStream(new ByteArrayInputStream(("{1,2}").getBytes())));

        //词法分析器转符号流
        CommonTokenStream commonTokenStream = new CommonTokenStream(arrayInitLexer);
        commonTokenStream.fill();

        //打印所有的词法符号
        for (Token token : commonTokenStream.getTokens()) {
            System.out.println(token);
        }

        //创建语法分析器
        ArrayInitParser arrayInitParser = new ArrayInitParser(commonTokenStream);

        //调用根语法得到语法树
        ArrayInitParser.InitContext context = arrayInitParser.init();

        //使用工具呈现数结构
        Trees.inspect(context, arrayInitParser);

    }

}
