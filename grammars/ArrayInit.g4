/**
* 数组初始化语法
*/
grammar ArrayInit;

//init规则格式 {整数,{整数,{整数},整数},整数}
init:'{' value (',' value)* '}';

//值语法规则,或init或整数词法
value:init|INT;

//定义词法规则
INT:[0-9]+;

//定义空白符号跳过
WS:[ \t\r\n]+->skip;